$(document).ready(function() {
    var controller = $.superscrollorama();

    /*CONCEPT*/
    controller.addTween('.tw_concept_head', TweenMax.from( $('.tw_concept_head'), .5, {
        opacity: 0,
        ease:Power2.easeIn
    }));
    controller.addTween('.tw_concept_text', TweenMax.from( $('.tw_concept_text'), .5, {
        opacity: 0,
        transform: 'translateY(50px)',
        ease:Power2.easeIn
    }));
    controller.addTween('.tw_concept_text', TweenMax.from( $('.tw_concept_photo'), .5, {
        opacity: 0,
        ease:Power2.easeIn
    }));

    /*BRAND*/
    controller.addTween('.tw_brand_wrapper', TweenMax.to( $('.tw_brand_wrapper'), 1.5, {
        transform: 'scaleX(0)',
        transformOrigin: 'right',
        delay: 0.2,
        ease: Expo.easeOut
    }));
    controller.addTween('.tw_brand_content', TweenMax.from( $('.tw_brand_content'), 2, {
        transform: 'translateX(-200px)',
        ease: Expo.easeOut
    }));

    controller.addTween('.tw_feature_head', TweenMax.from( $('.tw_feature_head'), 1, {
        opacity: 0,
        transform: 'translateY(100px)',
        ease:Elastic.easeOut
    }));
    controller.addTween('.tw_feature_caption', TweenMax.from( $('.tw_feature_caption'), 1, {
        opacity: 0,
        transform: 'translateY(100px)',
        ease:Elastic.easeOut
    }));

    controller.addTween('.tw_feature_text_fresh', TweenMax.to($('.tw_feature_fresh_wrapper'), 1.5, {
        transform: 'scaleX(0) scaleY(0.8)',
        transformOrigin: 'left',
        delay: 0.2,
        ease: Expo.easeOut
    }));
    controller.addTween('.tw_feature_text_fresh', TweenMax.from( $('.tw_feature_fresh'), 2, {
        transform: 'translateX(50px)',
        transformOrigin: 'right',
        ease: Expo.easeOut
    }));
    controller.addTween('.tw_feature_text_quality', TweenMax.to($('.tw_feature_quality_wrapper'), 1.5, {
        transform: 'scaleX(0) scaleY(0.8)',
        transformOrigin: 'right',
        delay: 0.2,
        ease: Expo.easeOut
    }));
    controller.addTween('.tw_feature_text_quality', TweenMax.from( $('.tw_feature_quality'), 2, {
        transform: 'translateX(-50px)',
        ease: Expo.easeOut
    }));
    controller.addTween('.tw_feature_text_selected', TweenMax.to($('.tw_feature_selected_wrapper'), 1.5, {
        transform: 'scaleX(0) scaleY(0.8)',
        transformOrigin: 'left',
        delay: 0.2,
        ease: Expo.easeOut
    }));
    controller.addTween('.tw_feature_text_selected', TweenMax.from( $('.tw_feature_selected'), 2, {
        transform: 'translateX(50px)',
        ease: Expo.easeOut
    }));
    controller.addTween('.tw_feature_text_fresh', TweenMax.from( $('.tw_feature_text_fresh'), 1, {
        opacity: 0,
        transform: 'translateY(50px)',
        ease: Expo.easeOut,
    }));
    controller.addTween('.tw_feature_text_quality', TweenMax.from( $('.tw_feature_text_quality'), 1, {
        opacity: 0,
        transform: 'translateY(50px)',
        ease: Expo.easeOut,
    }));
    controller.addTween('.tw_feature_text_selected', TweenMax.from( $('.tw_feature_text_selected'), 1, {
        opacity: 0,
        transform: 'translateY(50px)',
        ease: Expo.easeOut,
    }));

    /*PRODUCE*/
    controller.addTween('#tw_produce_wrapper', TweenMax.to( $('#tw_produce_wrapper'), 1, {
        transform: 'scaleX(0)',
        transformOrigin: 'right',
        ease: Expo.easeOut
    }));
    controller.addTween('#tw_produce_wrapper', TweenMax.from( $('.tw_produce_text'), .5, {
        opacity: 0,
        transfotabs: 'translateY(20px)',
        delay: 0.5,
        ease:Power2.easeIn
    }));
    controller.addTween('.tw_line', TweenMax.from( $('.tw_line'), .8, {
        transform: 'scaleX(0)',
        transformOrigin: 'left'
    }));
    
    /*BLEND*/
    controller.addTween('.tw_blend_head', TweenMax.from( $('.tw_blend_head'), .5, {
        opacity: 0,
        transform: 'translateY(30px) scale(1.1)',
        ease:Power2.easeIn
    }));
    controller.addTween('.tw_blend_text', TweenMax.from( $('.tw_blend_text'), .5, {
        opacity: 0,
        ease:Power2.easeIn
    }));
    
    /*BREW*/
    controller.addTween('.tw_brew_wrapper', TweenMax.to( $('.tw_brew_wrapper'), 2, {
        transform: 'scaleX(0)',
        transformOrigin: 'right',
        delay: 0.2,
        ease: Expo.easeOut
    }));
    controller.addTween('.tw_brew_photo', TweenMax.from( $('.tw_brew_photo'), 1, {
        transform: 'translateX(-100px)',
        ease: Expo.easeOut
    }));
    controller.addTween('.tw_brew_wrapper', TweenMax.from( $('.tw_brew_button'), .5, {
        opacity: 0,
        transform: 'translateY(50px)',
        delay: 1,
        ease: Power2.easeOut
    }));
    
    /*ORDER*/
    controller.addTween('.tw_order_head', TweenMax.from( $('.tw_order_head'), .5, {
        opacity: 0,
        transform: 'translateY(50px)',
        ease:Power2.ewseIn
    }));
    controller.addTween('.tw_order_box_01', TweenMax.from( $('.tw_order_box_01'), .5, {
        opacity: 0,
        transform: 'translateX(-50px)',
        ease:Power2.easeIn
    }));
    controller.addTween('.tw_order_box_02', TweenMax.from( $('.tw_order_box_02'), .5, {
        opacity: 0,
        transform: 'translateX(-50px)',
        delay: 0.5,
        ease:Power2.easeIn
    }));
    controller.addTween('.tw_order_box_03', TweenMax.from( $('.tw_order_box_03'), .5, {
        opacity: 0,
        transform: 'translateX(-50px)',
        delay: 1,
        ease:Power2.easeIn
    }));
    controller.addTween('.tw_order_box_04', TweenMax.from( $('.tw_order_box_04'), .5, {
        opacity: 0,
        transform: 'translateX(-50px)',
        delay: 1.5,
        ease:Power2.easeIn
    }));
    controller.addTween('.tw_order_box_01', TweenMax.from( $('.tw_order_button'), .5, {
        opacity: 0,
        delay: 2,
        ease:Power2.easeIn
    }));
    
    /*CONTACT*/
    controller.addTween('.tw_contact_mail', TweenMax.from( $('.tw_contact_mail'), 0.5, {
        opacity: 0,
        transform: 'translateX(-50px)',
        ease: Power2.easeIn
    }));
    controller.addTween('.tw_contact_twitter', TweenMax.from( $('.tw_contact_twitter'), 0.5, {
        opacity: 0,
        transform: 'translateX(50px)',
        ease: Power2.easeIn
    }));
    controller.addTween('.tw_contact_facebook', TweenMax.from( $('.tw_contact_facebook'), 0.5, {
        opacity: 0,
        transform: 'translateX(-50px)',
        ease: Power2.easeIn
    }));
    
    /*PRODUCE*/
    // 変数定義
    var cs       = document.getElementById('asia_line'),
        ctx      = cs.getContext('2d'),
        csWidth  = cs.width,
        csHeight = cs.height,
        center   = {
            x: csWidth / 2,
            y: csHeight / 2
        };

    // 線の基本スタイル
    ctx.strokeStyle = '#ffffff';
    ctx.lineWidth = 2;


    // 横線を引く
    var drawHorizontalLine = function() {
        ctx.beginPath();
        ctx.moveTo(0, center.y);
        ctx.lineTo(csWidth, center.y);
        ctx.closePath();
        ctx.stroke();
    };

    drawHorizontalLine();
    
    
    // 変数定義
    var cs       = document.getElementById('ncac_line'),
        ctx      = cs.getContext('2d'),
        csWidth  = cs.width,
        csHeight = cs.height,
        center   = {
            x: csWidth / 2,
            y: csHeight / 2
        };

    // 線の基本スタイル
    ctx.strokeStyle = '#ffffff';
    ctx.lineWidth = 2;


    // 横線を引く
    var drawHorizontalLine = function() {
        ctx.beginPath();
        ctx.moveTo(0, center.y);
        ctx.lineTo(csWidth, center.y);
        ctx.closePath();
        ctx.stroke();
    };

    drawHorizontalLine();
    
    
    // 変数定義
    var cs       = document.getElementById('sa_line'),
        ctx      = cs.getContext('2d'),
        csWidth  = cs.width,
        csHeight = cs.height,
        center   = {
            x: csWidth / 2,
            y: csHeight / 2
        };

    // 線の基本スタイル
    ctx.strokeStyle = '#ffffff';
    ctx.lineWidth = 2;


    // 横線を引く
    var drawHorizontalLine = function() {
        ctx.beginPath();
        ctx.moveTo(0, center.y);
        ctx.lineTo(csWidth, center.y);
        ctx.closePath();
        ctx.stroke();
    };

    drawHorizontalLine();
    
    
    // 変数定義
    var cs       = document.getElementById('africa_line'),
        ctx      = cs.getContext('2d'),
        csWidth  = cs.width,
        csHeight = cs.height,
        center   = {
            x: csWidth / 2,
            y: csHeight / 2
        };

    // 線の基本スタイル
    ctx.strokeStyle = '#ffffff';
    ctx.lineWidth = 2;


    // 横線を引く
    var drawHorizontalLine = function() {
        ctx.beginPath();
        ctx.moveTo(0, center.y);
        ctx.lineTo(csWidth, center.y);
        ctx.closePath();
        ctx.stroke();
    };

    drawHorizontalLine();
    
    /*スムーズスクロール*/
    // #で始まるアンカーをクリックした場合に処理
    $('a[href^=#]').click(function() {
        // スクロールの速度
        var speed = 1000; // ミリ秒
        // アンカーの値取得
        var href= $(this).attr("href");
        // 移動先を取得
        var target = $(href == "#" || href == "" ? 'html' : href);
        // 移動先を数値で取得
        var position = target.offset().top;
        // スムーススクロール
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });
    
    $(window).stellar({
        horizontalScrolling: true,
        verticalScrolling: true,
    });
    
});
